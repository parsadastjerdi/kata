#include <iostream>

int main() 
{
    long long int n, m, a;
    std::cin >> n >> m >> a;

    long long int length = n / a;
    long long int width = m / a;

    if (n % a > 0){
        ++length;
    } 

    if (m % a > 0) {
        ++width;
    }

    std::cout << length * width << std::endl;

    return 0;
}