#include <vector>
#include <iostream>

int sum(std::vector<int> n){
    int sum = 0;
    for (int i = 0; i < n.size(); ++i) sum += n.at(i);
    return sum;
}

int main() {
    int n,k,h;
    int min;
    std::cin >> n >> k;

    std::vector<int> maxHours(k,0);
    
    // first fill up first k elements of maxHours (k < n)
    for (int i = 0; i < k; ++i){
        std::cin >> h;
        maxHours.at(i) = h;
        if (i == 0) min = h;
        else if (h < min) min = h;
    }

    // replace any of the following times with 
    for (int i = 0; i < (n - k); ++i){
        std::cin >> h;
        if (h > min){
            for (int j = 0; j < maxHours.size(); ++j){
                if (maxHours.at(j) == min) maxHours.at(i) = h;
            }
        } 
    }

    std::cout << sum(maxHours) << std::endl;

    return 0;
}