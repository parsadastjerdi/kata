#include <iostream>
#include <vector>
#include <math.h>
#include <stdexcept>

using namespace std;

class Pumpkin{
public:
    // Default Constructor
    Pumpkin();

    // Fills pumpkin rectangle and throws exception if overlap occurs
    void fill(int x1, int y1, int x2, int y2);

private:
    vector< vector<bool> > face;
    int x;
    int y;
};

Pumpkin::Pumpkin(){
    this->x = this-> y = pow(10,5);
    // this takes a long time
    this->face = vector< vector<bool> > (this->x, vector<bool>(this->y, false));
}


void Pumpkin::fill(int x1, int y1, int x2, int y2){
    if (x2 <= x1 || y2 <= y1) throw std::exception();

    if (x1 > pow(10,5) || x1 > pow(10,5) || y1 > pow(10,5) || y2 > pow(10,5)) return;

    for (int i = x1; i < x2; ++i){
        for (int j = y1; j < y2; ++j){
            if (this->face.at(i).at(j) == true) throw std::exception();
            this->face.at(i).at(j) = true;
        }
    }
}


int main(){

    int numRectangles, x1, y1, x2, y2 = 0;
    bool error = false;

    std::cin >> numRectangles;

    Pumpkin p;

    for (int i = 0; i < numRectangles; ++i){
        std::cin >> x1 >> y1 >> x2 >> y2;
        try {
            p.fill(x1, y1, x2, y2);
        } catch(...){
            std::cout << "Yes" << std::endl;
            return 0;
        }
    }

    std::cout << "No" << std::endl;
    return 0;
}