#include <iostream>
#include <vector>
#include <string>

using namespace std;

int main(){
    int numIncorrect, numWords = 0;

    std::cin >> numWords;
    std::vector<string> words(numWords);

    // Fill up words vector
    for (int i = 0; i < numWords; ++i){
        std::cin >> words.at(i);
    }

    for (int i = 0; i < numWords; ++i) std::cout << words.at(i) << std::endl;

    return 0;
}


int main(){
    int numIncorrect, numWords = 0;

    std::cin >> numWords;
    std::vector<string> words(numWords);

    // Fill up words vector
    for (int i = 0; i < numWords; ++i){
        std::cin >> words.at(i);
        // std::cout << words.at(i) << std::endl;
    }

    // Test to see that the meet sp(...)ky condition
    for (int i = 0; i < words.size(); ++i){

        // Check first two characters, if incorrect increment and check next word
        if (words.at(i).at(0) != 's' || words.at(i).at(1) != 'p') {
            if (words.at(i).at(0) != 'S' || words.at(i).at(1) != 'P'){
                ++numIncorrect;

                // std::cout << "SP: " << words.at(i) << ": " << numIncorrect << std::endl;
                words.at(i) = "XX";
                continue;
            }
        }
        else {
            // remove first two characters
            words.at(i).erase(0,2);
            // std::cout << words.at(i) << std::endl;
        }

        for (int i = 0; i < words.size(); ++i){
            std::cout << "SP: " << words.at(i) << std::endl;
        }

        // Check last two characters, if incorrect increment and check next word
        if (words.at(i).at(words.size() - 2) != 'k' || words.at(i).at(words.size() - 1) != 'y') {
           if (words.at(i).at(words.size() - 2) != 'K' || words.at(i).at(words.size() - 1) != 'Y'){
                ++numIncorrect;
                // std::cout << "KY: " << words.at(i) << ": " << numIncorrect << std::endl;
                words.at(i) = "XX";
                continue;
           }
        }
        else {  
            // remove last two characters
            words.at(i).erase(words.size() - 2, 2);
            // std::cout << words.at(i) << std::endl;
        }

        for (int  i = 0; i < words.size(); ++i){
            std::cout << "KY: " << words.at(i) << std::endl;
        }

        return 0;

        // Test to see if the 0's in the middle are correct
        for (int j = 0; j < words.at(i).size(); ++j){

            if (words.at(i) == "XX"){
                // std::cout << words.at(i) << std::endl;
                continue;
            }

            // If the character is a part of the set, then check next character, else ++n and break
            if (words.at(i).at(j) == '0' || words.at(i).at(j) == 'o' || words.at(i).at(j) == 'O') continue;
            else {
                ++numIncorrect;
                // std::cout << "OO: " << words.at(i) << ": " << numIncorrect << std::endl;
                break;
            }
        }
    }

    std::cout << numIncorrect << std::endl;
}