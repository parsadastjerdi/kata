#include <iostream>
#include <vector>
#include <stdexcept>

int main(){
    int numStores, numItems, itemPrice, maxPrice = 0;

    std::cin >> numStores;
    if (numStores < 0) throw std::exception();

    std::vector<int> prices(numStores, 0);

    // Fill up store with item price
    for (int store = 0; store < numStores; ++store){
        std::cin >> numItems;
        if (numItems < 0) throw std::exception();

        for (int item = 0; item < numItems; ++item){
            std::cin >> itemPrice;
            if (itemPrice < 0) throw std::exception();
            prices.at(store) += itemPrice;
        }
    }

    // Find maximum price within store
    for (int store = 0; store < numStores; ++store){
        if (store == 0) maxPrice = prices.at(store);
        else if (prices.at(store) > maxPrice) maxPrice = prices.at(store);
    }

    std::cout << maxPrice << std::endl;

    return 0;
}