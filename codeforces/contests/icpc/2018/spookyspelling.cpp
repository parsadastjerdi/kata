#include <iostream>
#include <vector>
#include <string>
#include <regex>

using namespace std;

int main(){
    int numIncorrect, numWords = 0;

    std::cin >> numWords;
    std::vector<string> words(numWords);

    for (int i = 0; i < numWords; ++i){
        std::cin >> words.at(i);
    }

    regex spooky("[sS][pP][oO0]{2,}[kK][yY]");

    for (int i = 0; i < words.size(); ++i){
        if (!regex_match(words.at(i), spooky)) ++numIncorrect;
    }

    std::cout << numIncorrect << std::endl;
    return 0;
}

/* Tests  */

/*
7
sSpPoo0kY
spoKy
sP0Okey
Sp0Oy
Sp00ky
SPOoou0kky
sp00o0ooKY
*/

/*
10 
sPKY
qPoO0y
RGhj
RLoOC
sPo00kY
DNoocg
Nk00I
Po00I
vc
sARK
*/
